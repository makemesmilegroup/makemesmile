package it.lundstedt.erik.makeMeSmile;


import android.content.Intent;
import android.net.Uri;


import android.os.Bundle;
import android.os.Looper;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import com.google.android.material.snackbar.Snackbar;
import java.util.Random;

import io.multimoon.colorful.ThemeColor;

public class MainActivity extends AppCompatActivity {

private volatile String gUrl;

private AppBarConfiguration mAppBarConfiguration;

@Override
protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_main);
	Toolbar toolbar = findViewById(R.id.toolbar);
	setSupportActionBar(toolbar);
	FloatingActionButton fab = findViewById(R.id.fab);
	fab.setOnClickListener(
			new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					btnOnClick(view);
				}
			});

	DrawerLayout drawer = findViewById(R.id.drawer_layout);
	NavigationView navigationView = findViewById(R.id.nav_view);
	// Passing each menu ID as a set of Ids because each
	// menu should be considered as top level destinations.
	mAppBarConfiguration =
			new AppBarConfiguration.Builder(
					R.id.nav_home,
					R.id.nav_gallery,
					R.id.nav_slideshow,
					R.id.nav_tools,
					R.id.nav_share,
					R.id.nav_send)
					.setDrawerLayout(drawer)
					.build();
	NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
	NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
	NavigationUI.setupWithNavController(navigationView, navController);
}

@Override
public boolean onCreateOptionsMenu(Menu menu) {
	// Inflate the menu; this adds items to the action bar if it is present.
	getMenuInflater().inflate(R.menu.main, menu);
	return true;
}

@Override
public boolean onSupportNavigateUp() {
	NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
	return NavigationUI.navigateUp(navController, mAppBarConfiguration)
			|| super.onSupportNavigateUp();
}

public void printToLog(String text) {
	System.out.println("debug:" + text);
	TextView debugOutput = findViewById(R.id.text_home);
	debugOutput.append("\r\n"+text);
}

public void btnOnClick(android.view.View view) {
    
    /*String[] list = {
      "https://vm.tiktok.com/4A5Ds9/",
      "https://vm.tiktok.com/4Yy89g/",
      "https://vm.tiktok.com/4UevPN/",
      "https://vm.tiktok.com/XVgL68/",
      "https://vm.tiktok.com/XV5aAT/",
      "https://vm.tiktok.com/XjQrr6/",
      "https://vm.tiktok.com/XMBpxo/",
      "https://vm.tiktok.com/XVXU57/",
      "https://vm.tiktok.com/XjCQr4/",
      "https://vm.tiktok.com/XjXEDJ/",
      "https://vm.tiktok.com/XjQ71T/",
      "https://vm.tiktok.com/XgvYED/",
      "https://vm.tiktok.com/XgQSUV/",
      "https://vm.tiktok.com/Xgxm3o/",
      "https://vm.tiktok.com/Xg3XHH/",
      "https://vm.tiktok.com/XgX1un/",
      "https://vm.tiktok.com/Xg3wfw/",
      "https://vm.tiktok.com/XgGmQW/",
      "https://vm.tiktok.com/XgEMby/",
      "https://vm.tiktok.com/XgqvRt/",
      "https://vm.tiktok.com/XeRfjN/",
      "https://vm.tiktok.com/HnW5aR/",
      "https://vm.tiktok.com/XeFj4x/",
      "https://vm.tiktok.com/xQDmYq/",
      "https://vm.tiktok.com/XgvYED/",
      "https://vm.tiktok.com/XgQSUV/",
      "https://vm.tiktok.com/Xgxm3o/",
      "https://vm.tiktok.com/Xg3XHH/",
      "https://vm.tiktok.com/XgX1un/",
      "https://vm.tiktok.com/Xg3wfw/",
      "https://vm.tiktok.com/XgGmQW/",
      "https://vm.tiktok.com/XgEMby/",
      "https://vm.tiktok.com/XgqvRt/",
      "https://vm.tiktok.com/4YCr7b/",
      "https://vm.tiktok.com/4rHxvy/",
      "https://vm.tiktok.com/4rt23D/",
      "https://vm.tiktok.com/VkpbgJ/",
      "https://vm.tiktok.com/VefH4y/",
      "https://vm.tiktok.com/VeCTa7/",
      "https://vm.tiktok.com/45VoHc/",
      "https://vm.tiktok.com/VjSTQ2/",
      "https://vm.tiktok.com/VaUnCS/",
      "https://vm.tiktok.com/9AwHWH/",
      "https://vm.tiktok.com/9y9xdh/",
      "https://vm.tiktok.com/Xee58W/",
      "https://vm.tiktok.com/XeRfjN/",
      "https://vm.tiktok.com/XeFj4x/",
      "https://vm.tiktok.com/Xee58W/",
      "https://vm.tiktok.com/9t5dc6/",
      "https://vm.tiktok.com/9tBNjq/",
      "https://vm.tiktok.com/9t45MW/",
      "https://vm.tiktok.com/9ykwkS/",
      "https://vm.tiktok.com/9yaBna/",
      "https://vm.tiktok.com/9yUePD/",
      "https://vm.tiktok.com/9tSFtA/",
      "https://vm.tiktok.com/9tDFHf/",
      "https://vm.tiktok.com/9txjfC/",
      "https://vm.tiktok.com/x8VsRP/",
      "https://vm.tiktok.com/9tfdMt/",
      "https://vm.tiktok.com/9y6FX3/",
      "https://vm.tiktok.com/9yBckT/",
      "https://vm.tiktok.com/9yMX8j/",
      "https://vm.tiktok.com/x83ChF/",
      "https://vm.tiktok.com/9yfRbS/",
      "https://vm.tiktok.com/QLHAUV/",
      "https://vm.tiktok.com/QeE6DX/",
      "https://vm.tiktok.com/9yB2TV/",
      "https://vm.tiktok.com/QUCAv4/",
      "https://vm.tiktok.com/QUEKhB/",
      "https://vm.tiktok.com/QagnqY/",
      "https://vm.tiktok.com/QabbLh/",
      "https://vm.tiktok.com/QaGPJK/",
      "https://vm.tiktok.com/Qaodtx/",
      "https://vm.tiktok.com/QxvNKb/",
      "https://vm.tiktok.com/QxERa8/",
      "https://vm.tiktok.com/QQLJnT/",
      "https://vm.tiktok.com/QQenAS/",
      "https://vm.tiktok.com/Qxnjha/",
      "https://vm.tiktok.com/QQ2xJB/",
      "https://vm.tiktok.com/QxotuE/",
      "https://vm.tiktok.com/QxKVe1/",
      "https://vm.tiktok.com/QxvhTF/",
      "https://vm.tiktok.com/QxwCRu/",
      "https://vm.tiktok.com/QEkew8/",
      "https://vm.tiktok.com/QEXS2d/",
      "https://vm.tiktok.com/QEqAw7/",
      "https://vm.tiktok.com/QEfkTa/",
      "https://vm.tiktok.com/QECtoW/"
    };
    */
	
	// works/\
	
	System.out.println("you pressed the button");//debug
	
	final Thread mainThread = Thread.currentThread();
	
	Thread getterThread =
			new Thread(
					new Runnable() {
						@Override
						public void run() {
							try {
								Looper.prepare();
								Random rnd = new Random();
								// Your code goes here
								System.out.println(mainThread.getState());
								String[] list = Net.get();
								
								System.out.println(rnd.nextInt(list.length));
								String url = list[rnd.nextInt(list.length)];
								System.out.println(url);
								
								//				String url=Net.newGet();
								//				printToLog(url);
								
								gUrl = url;

    				
							} catch (Exception e) {
								System.out.println("exeption: ");
								e.printStackTrace();
							}
							mainThread.interrupt();
						}
					});
	
	System.out.println("0:" + mainThread.getState());
	getterThread.start();
	System.out.println("1:" + mainThread.getState());
	
	System.out.println(gUrl);
	try {
		System.out.println("2:" + mainThread.getState());
		Thread.sleep(500000);
		System.out.println("3:" + mainThread.getState());
	} catch (InterruptedException e) {
		// e.printStackTrace();
		System.out.println(e.getMessage());
	}
	
	// System.out.println("length" + gUrl.length());
	printToLog(gUrl);
	System.out.println("main:: " + gUrl);

   
    Intent ttIntent = new Intent(Intent.ACTION_VIEW);
    ttIntent.setData(Uri.parse(gUrl));
    startActivity(ttIntent);
    
}
}
